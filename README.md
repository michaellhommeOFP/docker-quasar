# Information

A docker container designed to run [Quasar](https://quasar.dev/) for **development** purpose. Comes with an helper script `quasar` to run directly the commands within the container

# Usage

You can use the `quasar` helper script as the original `quasar` commands : to start the web server, use `quasar dev` from your project directory, it will use the default port 8080 (you can change the port by the `QUASAR_PORT` environment variable)

Additional commands:
- `quasar yarn ...` to execute yarn in the project directory
- `quasar console` to start a shell in the container

# Pre-built image

If no adjustments are needed (node version), you can use the deployed image from the GitLab Container Registry. Simply download the helper scripts and move them somewhere accessible by your shell (eg: `/usr/local/bin`)

# Build the image

If you need to adjust the image (to change the node version), you can build the image with the following instructions. The helper scripts then needs to be adjusted to replace `registry.gitlab.com/michaellhomme/docker-quasar` by the name of the image you build (`quasar` in the following examples)

```
docker build -t quasar .
```
