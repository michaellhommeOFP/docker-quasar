# Use node<16, node-sass doesn't support it yet (2021-06-15)
FROM node:14-alpine

# Setup container
RUN echo "Installing tools..." \
  && yarn global add @quasar/cli \
  && apk update \
  # for usermod/groudmod
  && apk add shadow \ 
  # node-sass build tools
  && apk add make g++ python3 \
  # other tools
  && apk add git

COPY quasar-su /usr/local/bin/quasar-su
RUN chmod a+x /usr/local/bin/quasar-su

# Entry point to the user script
ENTRYPOINT [ "sh", "/usr/local/bin/quasar-su" ]
